import pandas as pd
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, and_
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from sqlalchemy.sql.expression import select

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# connect to server
from Kpi_data_helper import MleKpiDataRequestData, AllAccountDetails

engine = create_engine("mysql+pymysql://dbadmin:root@123@192.168.13.204:3307/appsone")



Base = declarative_base()


class Account(Base):
    __table__ = Table('account', Base.metadata, autoload=True, autoload_with=engine)


class TagMapping(Base):
    __table__ = Table('tag_mapping', Base.metadata, autoload=True, autoload_with=engine)


class MstTimezone(Base):
    __table__ = Table('mst_timezone', Base.metadata, autoload=True, autoload_with=engine)


class TagDetails(Base):
    __table__ = Table('tag_details', Base.metadata, autoload=True, autoload_with=engine)


class Controller(Base):
    __table__ = Table('controller', Base.metadata, autoload=True, autoload_with=engine)

class ConnectionDetails(Base):
    __table__ = Table('connection_details', Base.metadata, autoload=True, autoload_with=engine)

class CompClusterMappingDetails(Base):
    __table__ = Table('component_cluster_mapping', Base.metadata, autoload=True, autoload_with=engine)



# class ViewCompInstance(Base):
#     __table__ = Table('view_component_instance', Base.metadata, autoload=True, autoload_with=engine)




"""\
            session.select([Account.id.label('accountId'), Account.name.label('accountName'), MstTimezone.timeoffset.label('timezoneMilli'), Account.identifier.label('identifier')], MstTimezone.time_zone_id.label('timeZoneString'), Account.updated_time.label('updatedTimeString'), Account.user_details_id.label('updatedBy') , MstTimezone.timeoffset.label('timeoffset'), MstTimezone.abbreviation.label('abbreviation'), offset_name.label('offsetName'), Account.user_details_id.label('userDetailsId')).\
            where(TagMapping.object_id == Account.id )
            
            
                    self.account_details_prepared = select([Account.id.label('accountId'), Account.name.label('accountName'), MstTimezone.timeoffset.label('timezoneMilli'), Account.identifier.label('identifier')], MstTimezone.time_zone_id.label('timeZoneString'), Account.updated_time.label('updatedTimeString'), Account.user_details_id.label('updatedBy') , MstTimezone.timeoffset.label('timeoffset'), MstTimezone.abbreviation.label('abbreviation'), MstTimezone.offset_name.label('offsetName'), Account.user_details_id.label('userDetailsId')).\
            where(and_(TagMapping.object_id == Account.id, TagMapping.object_ref_table == self.accountTableName, TagMapping.tag_id == TagDetails.id, TagDetails.name == self.timezoneKey, MstTimezone.id == TagMapping.tag_key, Account.status == 1))
"""


class AllAccountsDetailsService:

    def __init__(self,session):
        self.session = session




    def get_account_details(self, accountTableName = "account", timezoneKey = 'Timezone'):
        """ For Fetching Account details """

        sess = self.session()
        account_details_prepared = sess.query(Account.id.label('accountId'), Account.name.label('accountName'), MstTimezone.timeoffset.label('timezoneMilli'), Account.identifier.label('identifier'), MstTimezone.time_zone_id.label('timeZoneString'), Account.updated_time.label('updatedTimeString'), Account.user_details_id.label('updatedBy') , MstTimezone.timeoffset.label('timeoffset'), MstTimezone.abbreviation.label('abbreviation'), MstTimezone.offset_name.label('offsetName'), Account.user_details_id.label('userDetailsId')).\
            filter(and_(TagMapping.object_id == Account.id, TagMapping.object_ref_table == accountTableName, TagMapping.tag_id == TagDetails.id, TagDetails.name == timezoneKey, MstTimezone.id == TagMapping.tag_key, Account.status == 1))
        resultProxy = account_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    def getTagMappingDetails(self, curr_account_id):
        sess = self.session()
        tag_mapping_details_prepared = sess.query(TagMapping.id.label('id'),TagMapping.tag_id.label('tagId'), TagMapping.object_id.label('objectId') ,TagMapping.object_ref_table.label('objectRefTable') ,TagMapping.tag_key.label('tagKey') ,TagMapping.tag_value.label('tagValue') ,TagMapping.created_time.label('createdTime') ,

                                    TagMapping.updated_time.label('updatedTime'), TagMapping.account_id.label('accountId'), TagMapping.user_details_id.label('userDetailsId')). \
                                    filter(TagMapping.account_id.in_([curr_account_id]))
        resultProxy = tag_mapping_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    def getConnectionDetails(self, curr_account_id):
        sess = self.session()
        connection_details_prepared = sess.query(ConnectionDetails.id.label('id'), ConnectionDetails.source_id.label('sourceId'), ConnectionDetails.source_ref_object.label('sourceRefObject'), ConnectionDetails.destination_id.label('destinationId'), ConnectionDetails.destination_ref_object.label('destinationRefObject'),
                                        ConnectionDetails.account_id.label('accountId'), ConnectionDetails.user_details_id.label('userDetailsId')). \
                                        filter(ConnectionDetails.account_id.in_([curr_account_id]))
        resultProxy = connection_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    # def getCompInstClusterDetails(self, curr_account_id):
    #     sess = self.session()
    #     comp_inst_cluster_details_prepared = sess.query(ViewCompInstance.id.label('instanceId'), ViewCompInstance.common_version_id.label('commonVersionId'), ViewCompInstance.mst_component_id.label('compId'), ViewCompInstance.component_name.label('componentName'), ViewCompInstance.mst_component_type_id.label('mstComponentTypeId'),
    #                                             ViewCompInstance.component_type_name.label('componentTypeName'),
    #                                             ViewCompInstance.mst_component_version_id.label('compVersionId'), ViewCompInstance.component_version_name.label('componentVersionName'), ViewCompInstance.name.label('instanceName'), ViewCompInstance.host_id.label('hostId') , ViewCompInstance.status.label('status'),
    #                                             ViewCompInstance.host_name.label('hostName'), ViewCompInstance.is_cluster.label('isCluster'), ViewCompInstance.identifier, ViewCompInstance.host_address.label('hostAddress'), ViewCompInstance.parent_instance_id.label('parentInstanceId')). \
    #                                         filter(ViewCompInstance.account_id.in_([curr_account_id]))
    #     resultProxy = comp_inst_cluster_details_prepared.all()
    #     result_df = pd.DataFrame(resultProxy)
    #     return result_df

    def getCompInstClusterDetails(self, curr_account_id):
        query_text = text(
            "select id instanceId,common_version_id commonVersionId,mst_component_id compId,component_name componentName,mst_component_type_id mstComponentTypeId," \
            "component_type_name componentTypeName," \
            "mst_component_version_id compVersionId,component_version_name componentVersionName,name instanceName,host_id hostId,status," \
            "host_name hostName,is_cluster isCluster,identifier,host_address hostAddress,parent_instance_id parentInstanceId from view_component_instance " \
            "where account_id in (:accountId)")

        with engine.connect() as con:
            resultProxy = con.execute(query_text, accountId=curr_account_id)

        result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df

    def getCompKpisMappingDetails(self, curr_account_id):
        query_text = "select mst_component_id componentId,mst_common_version_id mstCommonVersionId," \
                            "kpi_id kpiId,kpi_name kpiName,kpi_identifier kpiIdentifier, do_analytics doAnalytics, status" \
                            "  from view_common_version_kpis where account_id in ({},{})".format(str(curr_account_id), '1')

        result_df = pd.read_sql(query_text, engine)

        # with engine.connect() as con:
        #     resultProxy = con.execute(query_text, account_id= [curr_account_id, 1])
        #
        # result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df


    def getTagDetails(self, curr_account_id):
        sess = self.session()
        tag_details_prepared = sess.query(TagDetails.id, TagDetails.name, TagDetails.tag_type_id.label('tagTypeId'), TagDetails.is_predefined.label('isPredefined'), TagDetails.ref_table.label('refTable'), TagDetails.created_time.label('createdTime'), TagDetails.updated_time.label('updatedTime'),
            TagDetails.account_id.label('accountId'), TagDetails.user_details_id.label('userDetailsId'), TagDetails.ref_where_column_name.label('refWhereColumnName'), TagDetails.ref_select_column_name.label('refSelectColumnName')). \
            filter(TagDetails.account_id.in_([curr_account_id, 1]))
        resultProxy = tag_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    def getTagDetailsGivenTagName(self, tagName):
        sess = self.session()
        tag_details_prepared = sess.query(TagDetails.id, TagDetails.name, TagDetails.tag_type_id.label('tagTypeId'),
                                          TagDetails.is_predefined.label('isPredefined'),
                                          TagDetails.ref_table.label('refTable'),
                                          TagDetails.created_time.label('createdTime'),
                                          TagDetails.updated_time.label('updatedTime'),
                                          TagDetails.account_id.label('accountId'),
                                          TagDetails.user_details_id.label('userDetailsId'),
                                          TagDetails.ref_where_column_name.label('refWhereColumnName'),
                                          TagDetails.ref_select_column_name.label('refSelectColumnName')). \
            filter(TagDetails.account_id.in_([1]))
        resultProxy = tag_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    # @RegisterMapperFactory(BeanMapperFactory. class )
    # @ SqlQuery("select id,name,tag_type_id tagTypeId,is_predefined isPredefined,ref_table refTable,created_time createdTime,updated_time updatedTime," +
    #
    # "account_id accountId,user_details_id userDetailsId,ref_where_column_name refWhereColumnName,ref_select_column_name refSelectColumnName " +
    # "from tag_details where name = :name and account_id in (<accountId>)")
    # TagDetailsBean
    # getTagDetails( @ Bind("name")
    # String
    # name,
    #
    # @BindIn("accountId")
    #
    # List < Integer > accountId);



    def getAllKpisDetails(self):
        query_text = text("select kpiid kpiId,kpi_name kpiName,description kpiDescription,measure_units kpiUnits,cluster_operation " \
            "clusterOperation,kpi_type kpiType,ifnull(group_id,0) groupId,group_name groupName, " \
              "group_identifier groupIdentifier, " \
            "ifnull(discovery,0) isDiscovery,ifnull(group_status,0) groupStatus, rollup_operation " \
            "rollupOperation, cluster_aggregation_type clusterAggType, instance_aggregation_type " \
            "instanceAggType, identifier identifier from view_all_kpis")

        with engine.connect() as con:
            resultProxy = con.execute(query_text)

        result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df

    def getAllTypesDetails(self):
        query_text = text("select type typeName, typeid typeId, name subTypeName, subtypeid subTypeId from view_types")

        with engine.connect() as con:
            resultProxy = con.execute(query_text)

        result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df




    def getClusterInstanceMappingDetails(self, curr_account_id):
        sess = self.session()
        comp_cluster_mapping_details_prepared = sess.query(CompClusterMappingDetails.id, CompClusterMappingDetails.comp_instance_id.label('compInstanceId'), CompClusterMappingDetails.cluster_id.label('clusterId')). \
            filter(CompClusterMappingDetails.account_id.in_([curr_account_id]))
        resultProxy = comp_cluster_mapping_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df

    def getApplicationDetails(self, curr_account_id):
        sess = self.session()
        application_details_prepared = sess.query(Controller.id.label('appId'),Controller.name.label('name'), Controller.controller_type_id.label('controllerTypeId'), Controller.identifier.label('identifier') , Controller.monitor_enabled.label('monitoringEnabled'),

                                                    Controller.account_id.label('accountId') , Controller.status). \
                                                    filter(Controller.account_id.in_([curr_account_id, 1]))
        resultProxy = application_details_prepared.all()
        result_df = pd.DataFrame(resultProxy)
        return result_df




    def getTxnAndGroupDetails(self, curr_account_id):
        query_text = text("select a.id txnId,a.name txnName,a.status,a.audit_enabled isAutoEnabled,a.is_autoconfigured isAutoConfigured,a.user_details_id userDetailsId,m.name as transactionTypeName,a.pattern_hashcode patternHashCode,a.description,a.identifier, a.account_id accountId, a.is_business_txn isBusinessTransaction, (select group_concat(txn_group_id) from transaction_group_mapping where object_id=a.id and object_ref_table='transaction' ) tagListString from transaction a, mst_sub_type m where a.status = 1 AND a.monitor_enabled = 1 AND m.id = a.transaction_type_id  AND a.account_id = :accountId")

        with engine.connect() as con:
            resultProxy = con.execute(query_text, accountId=curr_account_id)

        result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df

    def getWindowProfileDetails(self, curr_account_id):
        query_text = "select profile_id profileId, profile_name profileName, day_option_id dayOptionId, day_option_name dayOptionName, status status, " \
            "account_id accountId, day day, start_hour startHour, start_minute startMinute, end_hour endHour, end_minute endMinute, " \
            "is_business_hour isBusinessHour from view_coverage_window_profile_details where account_id in ({}, {})".format(str(curr_account_id), '1')

        result_df = pd.read_sql(query_text, engine)

        # with engine.connect() as con:
        #     resultProxy = con.execute(query_text, account_id=curr_account_id)
        #
        # result_df = pd.DataFrame(resultProxy, columns=resultProxy._metadata.keys)
        return result_df


















def validate_given_input(curr_fromTime, curr_toTime):
    if curr_fromTime < curr_toTime:
        return True
    else:
        message = 'Given from time greater than to time.'
        raise Exception(message)

def validate_current_account_id(curr_accountId, all_accs_info_df):

    #curr_account_present_flag = all_accs_info_df.identifier.isin([curr_accountId]).any()
    curr_acc_details = all_accs_info_df.loc[all_accs_info_df.identifier == curr_accountId]

    if not curr_acc_details.empty:
        return curr_acc_details

    else:
        message = 'Current Application id not present in list of configured accounts.'
        raise Exception(message)

def validate_curr_comp_inst_id(curr_comp_inst_cluster_details_df, curr_componentInstanceId):
    curr_comp_inst_details = curr_comp_inst_cluster_details_df.loc[curr_comp_inst_cluster_details_df.instanceId == curr_componentInstanceId]

    if not curr_comp_inst_details.empty:
        return curr_comp_inst_details

    else:
        message = 'Current Component instance id not present in list of configured comp instances.'
        raise Exception(message)

def filterComponentKpisList(componentKpis_df, kpisToFilterList, all_kpis_details_df):
    filteredKpi_df = None
    if len(kpisToFilterList) >0:
        kpisToFilterList = list(map(lambda x: int(x), kpisToFilterList))
        componentKpisToFilter_df = componentKpis_df.loc[componentKpis_df.kpiId.isin(kpisToFilterList)]

        filteredKpi_df = all_kpis_details_df.loc[all_kpis_details_df.kpiId.isin(list(componentKpisToFilter_df.kpiId))]

    else:
        filteredKpi_df = all_kpis_details_df.loc[all_kpis_details_df.kpiId.isin(list(componentKpis_df.kpiId))]
        if len(componentKpis_df) != len(filteredKpi_df):
            message = 'Invalid kpi received for filter, required kpi list'
            raise Exception(message)

    return filteredKpi_df.loc[filteredKpi_df.kpiType != "Availability"]

def getMappedServiceListOfInstance(allAccountDetails_obj, currRequestData_obj):

    accountId = currRequestData_obj.curr_accountId
    instanceId = currRequestData_obj.curr_componentInstanceId

    allInstances_df= allAccountDetails_obj.CompInstClusterDetails
    allClusterInstanceMapping_df = allAccountDetails_obj.ClusterInstanceMapping
    allTagDetails_df = allAccountDetails_obj.TagDetails
    allServices_df = allAccountDetails_obj.ApplicationDetails
    allTypesDetails_df = allAccountDetails_obj.AllTypesDetails

    # get all cluster id of given instance
    clusterIdList = list(allClusterInstanceMapping_df.loc[allClusterInstanceMapping_df.compInstanceId == instanceId].clusterId)

    #
    tag_name = "Controller"
    comp_inst_table_name = "comp_instance"

    details_of_controller_tag = allTagDetails_df.loc[(allTagDetails_df.name == tag_name) & (allTagDetails_df.accountId ==1)]
    controller_tag_id = int(details_of_controller_tag.id)

    clusterTagDetails = allTagDetails_df.loc[(allTagDetails_df.id == controller_tag_id) & (allTagDetails_df.refTable.str.lower() == comp_inst_table_name) & (allTagDetails_df.isPredefined.isin(clusterIdList))]

    #service_type_details_df = allTypesDetails_df.loc[]

# Optional<ViewTypes> subTypeOptional = HealUICache.INSTANCE.viewAllTypes
#                     .get(Constants.ALL_TYPES)
#                     .stream()
#                     .filter(it -> (typeName.trim().equalsIgnoreCase(it.getTypeName())))
#                     .filter(it -> (subTypeName.trim().equalsIgnoreCase(it.getSubTypeName())))
#                     .findAny();








if __name__ == '__main__':

    curr_account_identifier = "qa-d681ef13-d690-4917-jkhg-6c79b-1"
    curr_componentInstanceId = 3
    doAnalyticsFlagString = "nor"
    curr_fromTime = 1610217000000
    curr_toTime = 1610218200000
    kpiListString = None



    """ Step 1. First validate given information """
    validate_given_input(curr_fromTime, curr_toTime)


    Session = sessionmaker(bind=engine)
    all_acc_details = AllAccountsDetailsService(Session)

    """Step 2: Fetching all accounts information details"""
    all_accs_info_df = all_acc_details.get_account_details()

    """step 3: From all accounts information verify the current account Id . if present get its details """
    curr_acc_details = validate_current_account_id(curr_account_identifier, all_accs_info_df)

    """step 4: Get the current_account_id from curr_acc_details"""
    curr_acc_id = int(curr_acc_details.accountId)

    """step 5: Now for this given curr_account_id get all Component Instance details for given account"""
    curr_comp_inst_cluster_details_df = all_acc_details.getCompInstClusterDetails(curr_acc_id)

    """step 6: Now validate the curr_componentInstanceId and get all its details"""
    curr_comp_inst_details = validate_curr_comp_inst_id(curr_comp_inst_cluster_details_df, curr_componentInstanceId)

    """step 7: Set All Current MLE Kpi Details into the MleKpiDataRequestData object"""
    request_data_dict = {
        "curr_account_details" :curr_acc_details,
        "curr_instance_details" : curr_comp_inst_details,
        "curr_componentInstanceId" :curr_componentInstanceId,
        "curr_accountId":curr_acc_id,
        "fromTime" : curr_fromTime,
        "toTime" : curr_toTime,
        "kpiListString" : kpiListString,
        "curr_account_identifier" : curr_account_identifier,
        "doAnalyticsFlagString" : doAnalyticsFlagString
    }
    curr_request_data_obj = MleKpiDataRequestData()
    curr_request_data_obj.set_params(request_data_dict)



    """step 8: Now given current account is configured. Get all the details of this current account"""
    "a) Tag Mapping details"
    curr_tag_mapping_details = all_acc_details.getTagMappingDetails(curr_acc_id)

    "b) Connection details"
    curr_connection_details = all_acc_details.getConnectionDetails(curr_acc_id)

    "c) Component Instance Details"
    curr_comp_inst_cluster_details = curr_comp_inst_cluster_details_df

    "d) Component Kpi Mapping Details"
    curr_comp_kpi_mapping_details = all_acc_details.getCompKpisMappingDetails(curr_acc_id)

    "e) Tag Details"
    curr_tag_details = all_acc_details.getTagDetails(curr_acc_id)

    "f) All KPIs Details"
    all_kpis_details = all_acc_details.getAllKpisDetails()

    "g) Cluster-Instance Mapping Details"
    curr_cluster_instance_map_details = all_acc_details.getClusterInstanceMappingDetails(curr_acc_id)

    "h) Txn and Group Details"
    curr_txn_and_group_details = all_acc_details.getTxnAndGroupDetails(curr_acc_id)

    "i) Window Profile Details"
    curr_window_profile_details = all_acc_details.getWindowProfileDetails(curr_acc_id)

    "j) Application Details"
    curr_application_details = all_acc_details.getApplicationDetails(curr_acc_id)

    "h) All types details"
    all_types_details = all_acc_details.getAllTypesDetails()



    """step 9: Set All Account details"""
    all_acc_dict = {
        "TagMappingDetails": curr_tag_mapping_details,
        "ConnectionDetails": curr_connection_details,
        "CompInstClusterDetails": curr_comp_inst_cluster_details,
        "ComponentKpis" : curr_comp_kpi_mapping_details,
        "TagDetails": curr_tag_details,
        "AllKpiList": all_kpis_details,
        "AllTypesDetails":all_types_details,
        "ClusterInstanceMapping": curr_cluster_instance_map_details,
        "TxnAndGroup": curr_txn_and_group_details,
        "WindowProfileBean": curr_window_profile_details,
        "ApplicationDetails":curr_application_details

    }
    all_acc_details_obj = AllAccountDetails()
    all_acc_details_obj.set_params(all_acc_dict)

    """step 10: Get Filtered Kpi List"""
    "a) Kpis to filter List"
    kpisToFilterList = []
    if kpiListString:
        kpisToFilterList = kpiListString.split(",")

    "b) Component Kpis List"
    commonVersionId = int(curr_comp_inst_details.compVersionId)
    componentKpis_df = curr_comp_kpi_mapping_details.loc[curr_comp_kpi_mapping_details.mstCommonVersionId == commonVersionId]

    "*) Filtering All Kpis List"
    filtered_list_df = filterComponentKpisList(componentKpis_df, kpisToFilterList, all_kpis_details)

    if filtered_list_df.empty:
        message = 'Invalid component instance id: {}'.format(str(curr_componentInstanceId))
        raise Exception(message)


    """step 11 :mleGetKpiData(filteredList,account,fromTime,toTime,
                AggregationLevel.MINUTELY, instanceDetails)"""
    # mleGetKpiData(filteredList, account, fromTime, toTime,
    #               AggregationLevel.MINUTELY, instanceDetails)
    "a) Get Mapped Services"
    all_mapped_service = getMappedServiceListOfInstance(all_acc_details_obj, curr_request_data_obj)















